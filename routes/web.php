<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SurveyController@homeloggedout');

Route::get('/create-survey', 'SurveyAdminController@home');
Route::post('/create-survey', 'SurveyAdminController@store');
Route::get('/survey/{slug}', 'SurveyController@show');
Route::get('/survey-created/{slug}', 'SurveyAdminController@success');
Route::get('/surveys', 'SurveyAdminController@showAll');
Route::post('/submit-answer/{id}', 'AnswerController@store');
Route::get('/survey-completed', 'AnswerController@success');
Route::get('/survey-results/{slug}', 'SurveyAdminController@showResults');
Route::get('/individual-result/{id}', 'SurveyAdminController@individualResult');
Route::get('/no-permission', 'HomeController@noPermission');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

