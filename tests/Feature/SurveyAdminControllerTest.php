<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Survey;
use App\Question;
use App\MultiChoiceAnswer;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SurveyAdminController extends TestCase
{
	 use DatabaseMigrations;
    public function testHome()
    {
        $user = factory(\App\User::class)->create();
        $response = $this->actingAs($user)->get('/create-survey');
        $response->assertStatus(200);
    }

    public function testHomeNeedsAuth()
    {
    	$response = $this->get('/create-survey');
        $response->assertStatus(302)
        		 ->assertRedirect('login');
    }

    public function testStore()
    {
    	$user = factory(\App\User::class)->create();
    	$response = $this->actingAs($user)->post('/create-survey', array(
    		'_token' => csrf_token(),
    		'name' => 'Test Name',
    		'question' => array(array('Text Question', 'text'),
    							array('Multi-choice Question', 'multi-choice', array('Answer 1', 'Answer 2')))
    		));
    	$survey = Survey::first();
    	$question = Question::all();
    	$multiChoiceAnswer = MultiChoiceAnswer::all();
    	$response->assertStatus(302)
    			 ->assertRedirect("/survey-created/{$survey->slug}");
    	$this->assertTrue($survey['name'] == 'Test Name', 'Survey creation broken');
    	$this->assertTrue($question[0]['question'] == 'Text Question', 'Question creation broken');
    	$this->assertTrue($question[0]['question_type'] == 'text', 'Question creation broken');
    	$this->assertTrue($question[1]['question'] == 'Multi-choice Question', 'Question creation broken');
    	$this->assertTrue($question[1]['question_type'] == 'multi-choice', 'Question creation broken');
    	$this->assertTrue($multiChoiceAnswer[0]['answer'] == 'Answer 1', 'Multi choice answer creation broken');
    	$this->assertTrue($multiChoiceAnswer[0]['question_id'] == 2, 'Multi choice answer creation broken');
    	$this->assertTrue($multiChoiceAnswer[1]['answer'] == 'Answer 2', 'Multi choice answer creation broken');
    	$this->assertTrue($multiChoiceAnswer[1]['question_id'] == 2, 'Multi choice answer creation broken');
  	}

  	public function testSuccess()
  	{
  		$user = factory(\App\User::class)->create();
    	$survey = factory(\App\Survey::class)->create();
  		$response = $this->actingAs($user)->get("/survey-created/{$survey->slug}");
  		$response->assertStatus(200)
  		         ->assertSee("localhost:8000/survey/{$survey->slug}");
  	}

  	public function testShow()
  	{
  		$user = factory(\App\User::class)->create();
  		$survey = factory(\App\Survey::class, 2)->create();
  		$response = $this->actingAs($user)->get('/surveys');
  		$response->assertStatus(200)
  				 ->assertSee("{$survey[0]['name']}")
  				 ->assertSee("{$survey[1]['name']}")
  				 ->assertSee("{$survey[0]['slug']}")
  				 ->assertSee("{$survey[1]['slug']}");
  	}

    public function testShowResults()
    {
        $user = factory(\App\User::class)->create();
        $survey = factory(\App\Survey::class)->create();
        $multiChoiceQuestion = factory(\App\Question::class)->states('multi-choice')->create();
        $textQuestion = factory(\App\Question::class)->states('text')->create();
        $multiChoiceAnswer = factory(\App\MultiChoiceAnswer::class)->create();
        $respondent = factory(\App\Respondent::class)->create();
        $response = $this->actingAs($user)->get("/survey-results/{$survey->slug}");
        $response->assertStatus(200)
                 ->assertSee($textQuestion['question'])
                 ->assertSee($multiChoiceQuestion['question'])
                 ->assertSee('View Individual Responses');
    }

    public function testShowResultsOnlyAccessibleToOwner()
    {
        $user = factory(\App\User::class)->create();
        $user2 = factory(\App\User::class)->create();
        $survey = factory(\App\Survey::class)->create();
        $multiChoiceQuestion = factory(\App\Question::class)->states('multi-choice')->create();
        $textQuestion = factory(\App\Question::class)->states('text')->create();
        $multiChoiceAnswer = factory(\App\MultiChoiceAnswer::class)->create();
        $respondent = factory(\App\Respondent::class)->create();
        $response = $this->actingAs($user2)->get("/survey-results/{$survey->slug}");
        $response->assertStatus(302)
                 ->assertRedirect('/no-permission');
    }

    public function testIndividualResult()
    {
        $user = factory(\App\User::class)->create();
        $survey = factory(\App\Survey::class)->create();
        $textQuestion = factory(\App\Question::class)->states('text')->create();
        $respondent = factory(\App\Respondent::class)->create();
        $textAnswer = factory(\App\Answer::class)->create();
        $response = $this->actingAs($user)->get("/individual-result/{$respondent->id}");
        $response->assertStatus(200)
                 ->assertSee($textAnswer['answer']);
    }

    public function testIndividualResultOnlyAccessibleToOwner()
    {
        $user = factory(\App\User::class)->create();
        $user2 = factory(\App\User::class)->create();
        $survey = factory(\App\Survey::class)->create();
        $textQuestion = factory(\App\Question::class)->states('text')->create();
        $respondent = factory(\App\Respondent::class)->create();
        $textAnswer = factory(\App\Answer::class)->create();
        $response = $this->actingAs($user2)->get("/individual-result/{$respondent->id}");
        $response->assertStatus(302)
                 ->assertRedirect('/no-permission');
    }
}