<?php

namespace Tests\Feature;

use App\Answer;
use App\MultiChoiceAnswer;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AnswerControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function testStore()
    {
        $user = factory(\App\User::class)->create();
        $survey = factory(\App\Survey::class)->create();
        $multiChoiceQuestion = factory(\App\Question::class)->states('multi-choice')->create();
        $textQuestion = factory(\App\Question::class)->states('text')->create();
        $multiChoiceAnswer = factory(\App\MultiChoiceAnswer::class)->create();
        $response = $this->post("/submit-answer/{$survey->id}", array(
            '_token' => csrf_token(),
            'answer' => array(1 => '1', 2 => 'Test Answer')
            ));
        $answer = Answer::all();
        $test = MultiChoiceAnswer::all();
        $response->assertStatus(302)
                 ->assertRedirect("/survey-completed");
        $this->assertTrue($answer[0]['answer'] == $multiChoiceAnswer['answer'], 'Answer section broken');
        $this->assertTrue($answer[1]['answer'] == 'Test Answer', 'Answer section broken');
    }

    public function testSuccess()
    {
    	$response = $this->get('/survey-completed');
    	$response->assertStatus(200)
    			 ->assertSee('Thank you for completing the survey.');
    }
}