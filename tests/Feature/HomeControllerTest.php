<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HomeControllerTest extends TestCase
{
	use DatabaseMigrations;
    public function testIndex()
    {
    	$user = factory(\App\User::class)->create();
    	$response = $this->actingAs($user)->get('/home');
    	$response->assertStatus(200)
    			 ->assertSee('You are logged in!');
    }
    public function testNoPermission()
    {
    	$user = factory(\App\User::class)->create();
    	$response = $this->actingAs($user)->get('/no-permission');
    	$response->assertStatus(200)
    			 ->assertSee('Opps');
    }
}
