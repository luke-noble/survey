<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SurveyControllerTest extends TestCase
{
	use DatabaseMigrations;

    public function testShow()
    {
    	$user = factory(\App\User::class)->create();
    	$survey = factory(\App\Survey::class)->create();
    	$question = factory(\App\Question::class, 5)->create();
    	$response = $this->get("/survey/{$survey->slug}");
        $response->assertStatus(200)
        		 ->assertSee($survey->name)
        		 ->assertSee($question[0]->question);
    }

    public function testHomeLoggedOut()
    {
    	$response = $this->get('/');
    	$response->assertStatus(200)
    			 ->assertSee('Please log in to begin');
    }
}
