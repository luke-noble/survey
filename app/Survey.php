<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $fillable = ['name', 'slug', 'user_id'];

    public function question()
    {
    	return $this->hasMany('App\Question');
    }

    public function respondent()
    {
    	return $this->hasMany('App\Respondent');
    }

    public static function addSlugToSurvey($request)
    {
    	do {
            $slug = str_random(20);
            $doesSlugExist = Survey::whereSlug($slug)->exists();
        } while($doesSlugExist);
        $request['slug'] = $slug;
        return $request;
    }
}
