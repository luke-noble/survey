<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Survey;

class SurveyController extends Controller
{
    public function show($slug)
    {
        $survey = Survey::where('slug', $slug)->first();
    	$questions = Question::where('survey_id', $survey->id)->get();
    	return view('answer-survey.survey', compact('questions'));
    }

    public function homeloggedout()
    {
        return view('index');
    }
}
