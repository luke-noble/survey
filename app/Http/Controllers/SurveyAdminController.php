<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Survey;
use App\MultiChoiceAnswer;
use App\Respondent;
use App\Answer;
use Charts;

class SurveyAdminController extends Controller
{
	public function __construct()
    {
    	$this->middleware('auth');
    }

    public function home()
    {
        return view('create-survey.create');
    }
    public function store()
    {
    	$this->validate(request(), [
            'name' => 'required|unique:surveys',
            ]);

        $request = \Request::all();
        $request = Survey::addSlugToSurvey($request);
        $survey = Survey::create([
            'name' => request('name'),
            'slug' => $request['slug'],
            'user_id' => auth()->id()
        ]);
        foreach($request['question'] as $question) {
            $currentQuestion = Question::create([
                'survey_id' => $survey->id,
                'question' => $question[0],
                'question_type' => $question[1]
            ]);
            if ($question[1] == 'multi-choice') {
                foreach ($question[2] as $answer) {
                    MultiChoiceAnswer::create([
                        'question_id' => $currentQuestion->id,
                        'answer' => $answer
                    ]);
                }
            }
        }
    	return redirect('/survey-created/' . $survey->slug);
    }

    public function success($slug)
    {
        return view('create-survey.success', compact('slug'));
    }

    public function showAll()
    {
        $surveys = Survey::where('user_id', auth()->id())->get();
        return view('view-survey.showall', compact('surveys'));
    }

    public function showResults($slug)
    {
        $survey = Survey::where('slug', $slug)->first();
        if(auth()->id() == $survey['user_id'])
        {
            $multiChoiceQuestions = Question::where([['survey_id', $survey['id']],
                                                    ['question_type', 'multi-choice']])->get();
            foreach($multiChoiceQuestions as $multiChoiceQuestion)
            {
                $charts[] = Charts::database(Answer::where('question_id', $multiChoiceQuestion['id'])->get(), 'bar', 'highcharts')
                            ->groupBy('answer')
                            ->title($multiChoiceQuestion['question'])
                            ->elementLabel('Number of Responses');
                
            }
            $respondents = Respondent::where('survey_id', $survey['id'])->get();
            return view('results-survey.show', compact(['survey', 'respondents', 'charts']));
        } else {
            return redirect('/no-permission');
        }
    }

    public function individualResult($id)
    {
        $answers = Answer::where('respondent_id', $id)->get();
        if($answers[0]->question->survey->user_id == auth()->id()){
            return view('results-survey.individual', compact('answers'));
        } else {
            return redirect('/no-permission');
        }
    }
}
