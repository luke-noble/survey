<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Answer;
use App\Survey;
use App\Respondent;
use App\Question;
use App\MultiChoiceAnswer;

class AnswerController extends Controller
{
    public function store($id)
    {
      $request = \Request::all();
    	$this->validate(request(), [
            'answer' => 'required',
            ]);
        $respondent = Respondent::create([
            'survey_id' => $id
            ]);
        foreach($request['answer'] as $questionid => $answer){
            $questionType = Question::where('id', $questionid)->first();
            if($questionType['question_type'] == 'text'){
    	       Answer::create([
    		      'answer' => $answer,
    		      'question_id' => $questionid,
                  'respondent_id' => $respondent['id'],
    		  ]);
           } elseif ($questionType['question_type'] == 'multi-choice'){
                $multiAnswer = MultiChoiceAnswer::where('id', $answer)->first();
                Answer::create([
                  'answer' => $multiAnswer->answer,
                  'question_id' => $questionid,
                  'respondent_id' => $respondent['id'],
              ]);
           }
        }
    	return redirect('/survey-completed');
    }

    public function success()
    {
        return view('answer-survey.success');
    }

}
