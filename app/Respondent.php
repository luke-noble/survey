<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respondent extends Model
{
    protected $fillable = ['survey_id'];

    public function survey()
    {
    	return $this->belongsTo('App\Survey');
    }
}
