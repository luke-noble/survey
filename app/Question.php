<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['survey_id', 'question', 'question_type'];

    public function survey()
    {
    	return $this->belongsTo('App\Survey');
    }

    public function answer()
    {
    	return $this->hasMany('App\Answer');
    }
    public function multiChoiceAnswer()
    {
    	return $this->hasMany('App\MultiChoiceAnswer');
    }
}
