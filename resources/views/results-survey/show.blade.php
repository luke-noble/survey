@extends('layouts.app')
@section('head')
{!! Charts::assets() !!}
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Survey Results</div>
                <div class="panel-body">
                <?php $q = 1; $c = 0; ?>
                    @foreach ($survey->question as $question)                            
                                <h1>Question {{$q}}: {{ $question->question }}</h1>
                                <table class="table table-striped">
                                @if ($question['question_type'] == 'text')
                                    <?php $a = 1 ?>
                                    <tr><th>ID</th><th>Answer</th></tr>
                                    @foreach ($question->answer as $answer)
                                        <tr><td>{{$a}}</td><td>
                                            {{ $answer->answer}}</td></tr>
                                        <?php $a++ ?>
                                    @endforeach
                                @elseif ($question['question_type'] == 'multi-choice')
                                    {!! $charts[$c]->render()!!}
                                    <?php $c++ ?>
                                @endif
                                </table>
                    <?php $q++ ?>
                    @endforeach
                    <table class="table table-striped">
                    <?php $r = 1 ?>
                        <tr><th>View Individual Responses</th></tr>
                        @foreach ($respondents as $respondent)
                            <tr><td>
                                <a href="/individual-result/{{$respondent->id}}">Respondent {{$r}}</a>
                            </td></tr>
                        <?php $r++ ?>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
