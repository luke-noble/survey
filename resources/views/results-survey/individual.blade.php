@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Individual Results</div>
                <div class="panel-body">
                <p><a href="/survey-results/{{$answers[0]->question->survey->slug}}"><- Back to results</a>
                    <table class="table table-striped">
                        <tr><th>Question</th><th>Answer</th></tr>
                            @foreach ($answers as $answer)
                                <tr><td>{{ $answer->question->question }}</td><td>{{ $answer->answer}}</td></tr>
                            @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
