@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">My Surveys</div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <tr>
                            <th>Survey Name</th>
                            <th>Survey Link</th>
                            <th>View Results</th>
                        <tr>
                        @foreach ($surveys as $survey)
                        <tr>
                            <td>{{ $survey->name }}</td>
                            <td><a href="/survey/{{ $survey->slug }}">Survey Link</td>
                            <td><a href="/survey-results/{{ $survey->slug }}">View results</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection