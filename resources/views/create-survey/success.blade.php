@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Survey Created</div>

                <div class="panel-body">
                    <p class="bg-success survey-create">Your survey was successfully created. It can be viewed at
                    <a href="/survey/{{$slug}}">{{config('app.url')}}/survey/{{$slug}}</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
