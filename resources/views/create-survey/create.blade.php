@extends('layouts.app')
@section('sidebar')
<div id="wrapper">
<!-- Sidebar -->
<div id="sidebar-wrapper">
    <ul class="sidebar-nav" style="margin-left:0;">
        <li class="sidebar-brand">
            Add Questions<a href="#menu-toggle"  id="menu-toggle" style="float:right;" ></a> 
        </li>
        <li class="sidebar-button-section">
            <button id="text-question" class="sidebar-question" onclick="addTextQuestion()">Free Text</button>
        </li>
        <li class="sidebar-button-section">
            <button id="multiple-choice-question" class="sidebar-question" onclick="addMultiQuestion()">Multiple Choice</button>
        </li>
        <li class="sidebar-button-section sidebar-submit">
            <input class="sidebar-submit-button" type="submit" form="create-survey" value="Create Survey">
        </li>
    </ul>
</div>
<!-- /#sidebar-wrapper -->
@endsection
@section('content')
<div class="container" id="page-content-wrapper">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create Survey</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="/create-survey" id="create-survey">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('surveyname') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Survey Name</label>
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required>
                            @if ($errors->has('name'))
                            <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div id="question-container" class="form-group">
                            <ul id="sortable">
                            </ul>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('specificScript')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script>
    var i = 1
    function addTextQuestion(){
        var div = document.createElement('div');
        div.className = 'form-group question-group question-number-' + i
        div.id = 'Q' + i
        div.innerHTML = '<i class="fa fa-arrows" aria-hidden="true"></i> <label for="question" class="control-label">Text Question</label><span class="delete-icon" onclick="removeAnswer('+i+')"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</span>'
         + '<input id="question" type="text" class="form-control" name="question[' + (i-1) +'][]" required>'
         + '<input type="hidden" name="question[' + (i-1) +'][]" value="text">'
         + '</div>';
        document.getElementById('sortable').appendChild(div);
        i++;
    }

    function addMultiQuestion(){
        var div = document.createElement('div');
        div.className = 'form-group question-group question-number-' + i
        div.id = 'Q' + i
        div.innerHTML = '<i class="fa fa-arrows" aria-hidden="true"></i> <label for="question" class="control-label">Multiple Choice Question</label><span class="delete-icon" onclick="removeAnswer('+i+')"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</span>'
         + '<div id="multi-choice-answer-container-' + i + '">'
         + '<input id="question" type="text" class="form-control multi-choice-question" name="question[' + (i-1) +'][]" required>'
         + '<input type="hidden" name="question[' + (i-1) +'][]" value="multi-choice">'
         + '<input type="radio"><input type="text" class="form-control multi-choice-answer" name="question[' + (i-1) +'][][]" required></div>'
         + '<div class="multi-choice-add-answer" id="add-answer-' + i + '" onclick="addAnswer('+i+')">+ Add another answer</div>';
        document.getElementById('sortable').appendChild(div);
        i++;
    }
    function addAnswer(k) {
        var div = document.createElement('div');
            div.class = '<div class="form-group">'
            div.innerHTML = '<input type="radio"><input type="text" class="form-control multi-choice-answer" name="question[' + (k-1) +'][2][]" required></div>'
            document.getElementById('multi-choice-answer-container-' + k).appendChild(div);
    }
    function removeAnswer(k) {
        var answer = '.question-number-' + k
        $(answer).remove();
    }
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    $(function() {
        $( "#sortable" ).sortable();
        $( "#sortable" ).disableSelection();
    });
</script>
@endsection