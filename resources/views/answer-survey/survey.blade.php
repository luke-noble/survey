@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $questions[0]->survey->name }}</div>

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="/submit-answer/{{ $questions[0]->survey->id}}">
                        {{ csrf_field() }}
                        @foreach($questions as $question)
                            @if ($question->question_type == 'text')
                            <div class="form-group{{ $errors->has('answer') ? ' has-error' : '' }}">
                                <label for="answer" class="control-label">{{ $question->question}}</label>
                                    <input id="answer" type="text" class="form-control" name="answer[{{$question->id}}]" value="{{ old('answer') }}" required>
                                    @if ($errors->has('answer'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('answer') }}</strong>
                                        </span>
                                    @endif
                            </div>
                            @elseif ($question->question_type == 'multi-choice')
                                <div class="form-group{{ $errors->has('answer') ? ' has-error' : '' }}">
                                <label for="answer" class="control-label">{{ $question->question}}</label><br>
                                    @foreach ($question->multiChoiceAnswer as $answer)
                                        <input id="answer" type="radio" class="" name="answer[{{$answer->question->id}}]" value="{{$answer->id}}" required> <span class="choice-answer">{{$answer->answer}}</span>
                                    @endforeach
                                    @if ($errors->has('answer'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('answer') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            @endif
                        @endforeach

                        <div class="form-group survey-button">
                                <button type="submit" class="btn btn-primary">
                                    Submit Answer
                                </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
