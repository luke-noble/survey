<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Survey::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence,
        'slug' => $faker->unique()->slug(1),
        'user_id' => 1
    ];
});

$factory->define(App\Question::class, function (Faker\Generator $faker) {
    return [
    	'survey_id' => 1,
        'question' => $faker->sentence,
    ];
});
$factory->state(App\Question::class, 'multi-choice', function ($faker) {
    return [
        'question_type' => 'multi-choice',
    ];
});
$factory->state(App\Question::class, 'text', function ($faker) {
    return [
        'question_type' => 'text',
    ];
});

$factory->define(App\Answer::class, function (Faker\Generator $faker) {
    return [
    	'question_id' => 1,
        'answer' => $faker->sentence,
        'respondent_id' => 1
    ];
});

$factory->define(App\MultiChoiceAnswer::class, function (Faker\Generator $faker) {
    return [
    	'question_id' => 1,
        'answer' => $faker->sentence,
    ];
});

$factory->define(App\Respondent::class, function (Faker\Generator $faker) {
    return [
    	'survey_id' => 1,
    ];
});